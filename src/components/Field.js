import React from 'react';
import { View, StyleSheet, Text } from 'react-native'
import params from '../params'
import Mine from './Mine'
import Flag from './Flag'

export default props => {
    const { mined, opened, nearMines, exploded, flagged } = props

    const styleField = [styles.field]
    //Incrementa estilo quando mina estiver aberta
    if (opened) styleField.push(styles.opened)
    //Caso a mina estiver explodida
    if (exploded) styleField.push(styles.exploded)
    //Verificação se o campo foi marcado com uma bandeira
    if (flagged) styleField.push(styles.flagged)
    //Incrementa o estilo quando o campo estiver no estado regular (padrão)
    if (!opened && !exploded) styleField.push(styles.regular)

    //Alterações de cor de acordo com minas próximas
    let color = null
    if (nearMines > 0 ){
        if (nearMines == 1) color = '#2A28D7'
        if (nearMines == 2) color = '#2B520F'
        if (nearMines > 2 && nearMines < 6) color = '#F9060A'
        if (nearMines >= 6) color = '#F221A9'
    }

    return (
        <View style={styleField}>
            {/** Se o campo não está minado (desenho da mina),
                se o campo está aberto 
                e se a qnt de minas proximas for > 0 */}
            {!mined && opened && nearMines > 0 ?
                <Text style={[styles.label, {color: color}]}>
                    {nearMines}</Text> : false
            }
            {/** Exibir mina explodida caso o campo estiver minada e aberto */}
            {mined && opened ? <Mine /> : false}
            { /** Exibição da Flag, caso o campo tenha a marcação e não estiver aberto */ }
            { flagged && !opened ? <Flag /> : false }
        </View>
    )
}

const styles = StyleSheet.create({
    field: {
        height: params.blockSize,
        width: params.blockSize,
        borderWidth: params.borderSize,
    },
    regular: {
        backgroundColor: '#999',
        borderLeftColor: '#CCC',
        borderTopColor: '#CCC',
        borderRightColor: '#333',
        borderBottomColor: '#333',
    },
    opened: {
        backgroundColor: '#999',
        borderColor: '#777',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        fontWeight: 'bold',
        fontSize: params.fontSize,
    },
    exploded: {
        backgroundColor: 'red',
        borderColor: 'red',
    },
    flagged: {

    }
})