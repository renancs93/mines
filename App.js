import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import params from './src/params'
import MineField from './src/components/MineField'
import {
  createMinedBoard
} from './src/functions'

export default class App extends Component {
  
  constructor(props){
    super(props)
    this.state = this.createState()
  }

  minesAmount = () =>{
    const cols = params.getCollumnsAmount()
    const rows = params.getRowsAmount()
    return Math.ceil(cols * rows * params.defficultLevel)
  }

  createState = () =>{
    const cols = params.getCollumnsAmount()
    const rows = params.getRowsAmount()
    return {
      board: createMinedBoard(rows, cols, this.minesAmount()),
    }
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Iniciando o Mines!</Text>
        <Text style={styles.instructions}> Tamanho da grade:
          {params.getRowsAmount()}x{params.getCollumnsAmount()}</Text>
        
        <View style={styles.board}>
          <MineField board={this.state.board} />
        </View>

          {/* Examplos * 
          <Field />
          <Field opened />
          <Field opened  nearMines={1}/>
          <Field opened  nearMines={2}/>
          <Field opened  nearMines={3}/>
          <Field opened  nearMines={6}/>
          <Field mined />
          <Field mined opened />
          <Field mined opened exploded />
          <Field flagged />
          <Field flagged opened />
          */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  board:{
    alignItems: 'center',
    backgroundColor: '#AAA'
  },
  
})
